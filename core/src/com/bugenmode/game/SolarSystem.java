package com.bugenmode.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class SolarSystem extends ApplicationAdapter {

    public static final int WIDTH = 640;
    public static final int HEIGHT = 480;
    public static final String TITLE = "Solar System";

    SpriteBatch batch;
    float angle = 120;
    Texture skyImage, sunImage, moonImage;
    float stateTime;
    Rectangle moon, planet1, planet2;
    int r = 3;
    float dx1 = 290, dy1 = 10;
    float dx2 = 290, dy2 = 360;
    float dx3 = 200, dy3 = 10;

    PlanetAnimation planetImage1 = new PlanetAnimation();
    PlanetAnimation planetImage2 = new PlanetAnimation();


    @Override
    public void create() {
        batch = new SpriteBatch();
        skyImage = new Texture("sky.png");
        sunImage = new Texture("sun.png");
        moonImage = new Texture("moon.png");
        moon = new Rectangle();
        planet1 = new Rectangle();
        planet2 = new Rectangle();

        planetImage1.PlanetAnimation("planet1.png", 5, 4);
        planetImage2.PlanetAnimation("planet2.png", 5, 4);
    }

    @Override
    public void render() {
        stateTime += Gdx.graphics.getDeltaTime();

        dx1 += (planet1.x + Math.cos(stateTime)) * r;
        dy1 += (planet1.y + Math.sin(stateTime)) * r;
        dx2 += (planet2.x - Math.cos(stateTime)) * r;
        dy2 += (planet2.y - Math.sin(stateTime)) * r;
        dx3 += (moon.x + Math.cos(stateTime)) * r;
        dy3 += (moon.y + Math.sin(stateTime)) * r;


        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        TextureRegion currentFrame1 = planetImage1.imgAnimation.getKeyFrame(stateTime, true);
        TextureRegion currentFrame2 = planetImage2.imgAnimation.getKeyFrame(stateTime, true);
        batch.begin();
        batch.draw(skyImage, 0, 0);
        batch.draw(sunImage, (WIDTH / 2) - (sunImage.getWidth() / 2), (HEIGHT / 2) - (sunImage.getHeight() / 2));
        batch.draw(moonImage, dx3, dy3, 50, 35);
        batch.draw(currentFrame1, dx1, dy1);
        batch.draw(currentFrame2, dx2, dy2);
        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
        skyImage.dispose();
        planetImage1.batch.dispose();
        planetImage2.batch.dispose();
        moonImage.dispose();
        sunImage.dispose();
    }

    public void resume() {

    }


    public void pause() {

    }

}