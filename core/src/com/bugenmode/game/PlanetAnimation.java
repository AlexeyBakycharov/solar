package com.bugenmode.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation;

public class PlanetAnimation {

    Animation<TextureRegion> imgAnimation;
    SpriteBatch batch;
    Texture img;
    float stateTime;

    public void PlanetAnimation(String objectName, int FRAME_COLS, int FRAME_ROWS){
        batch = new SpriteBatch();
        img = new Texture(Gdx.files.internal(objectName));
        TextureRegion[][] tmp = TextureRegion.split(img, img.getWidth() / FRAME_COLS, img.getHeight() / FRAME_ROWS);
        TextureRegion[] imgFrames = new TextureRegion[FRAME_ROWS * FRAME_COLS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++){
            for (int j = 0; j < FRAME_COLS; j++) {
                imgFrames[index++] = tmp[i][j];
                imgFrames[19] = tmp[0][0];
            }
        }
        imgAnimation = new Animation<TextureRegion>(0.05f, imgFrames);
        stateTime = 0f;
    }

}
