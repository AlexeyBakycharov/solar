package com.bugenmode.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.bugenmode.game.SolarSystem;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = SolarSystem.TITLE;
		config.width = SolarSystem.WIDTH;
		config.height = SolarSystem.HEIGHT;
		new LwjglApplication(new SolarSystem(), config);
	}
}
